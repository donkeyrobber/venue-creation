$(document).ready(function($){
 
$('#request-call').validate({

	rules: {

		first_name : {

			required : true,
			minlength : 3 

		},
		last_name : {

			required : true,
			minlength : 3

		},
		telephone : {

			required : true,
			minlength : 5

		},
		email : {

			required : true,
			email : true
		},
		comment : {

			required : false

		}

	},

	submitHandler : function(e){
		
		
		$.ajax({
			type : 'POST',
			url : 'http://api.venue-creation.co.uk/index.php/api/request-call',
			async: false,
        		dataType: 'json',
			crossDomain: true,
			data : $('#request-call').serialize()
		}).done(function(m){

			var $output;

			if( m ){

				if( $.isArray(m) ){

					var $output = $("<ul class=\"error\"></ul>");

					for(var i in m){	

					$output.append( $("<li><label>"+m[i]+"</label></li>") );

					}

				}else{

					$output = $("<label>"+m+"</label>");

				}
		
				$('#message').html($output);
			
			}

		});
		
		return false;
	}

});	


});
